class Matrix{

public:

	const int row;
	const int col;
	mt19937 rand2;
	const int size_m;
	int *matr;
	Matrix(int r, int c) :row(r), col(c), size_m(r*c){
		matr = new int[size_m];
		rand2.seed(0);
		int i = 0;
		for (; i < (r * c); i++){
			matr[i] = 1 + rand2() % 10;
		}
	}

	void display()
	{
		for (int i = 0; i < size_m; i++){
			cout << matr[i] << "\t";
			
			if ((i+1)%col  == 0)
				cout << endl;
		}
	}

};

